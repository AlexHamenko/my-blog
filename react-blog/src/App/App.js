import React, { Component } from 'react'
import './../common/normalize.css'
import './../common/base.css'

import Header from './Header/Header'
import Main from './Main/Main'
import Footer from './Footer/Footer'
import PersonalInfo from './PersonalInfo/PersonalInfo'

class App extends Component {
  render() {
    return (
      <div>
				<Header />
        <PersonalInfo />
				<Main />	
				<Footer />
			</div>
    );
  }
}

export default App;
