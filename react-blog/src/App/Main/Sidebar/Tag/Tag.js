import React from 'react'
import './tag.css';

const Tag = () => (
	<section class="tags">
		<h2>Tags</h2>
		<div class="tags-list">
			<span>Blog</span>
			<span>Personal</span>
			<span>Funny</span>
			<span>Project</span>
			<span>Practice</span>
			<span>Tag</span>
			<span>Dribble</span>
			<span>Behance</span>
			<span>Photo</span>
			<span>Color</span>
		</div>
	</section>
)

export default Tag