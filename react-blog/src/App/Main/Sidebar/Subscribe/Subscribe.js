import React from 'react'
import './subscribe.css';

const Subscribe = () => (
	<section className="subscribe">
		<h2>Stay in Touch</h2>
		<div className="subscribe-area">
			<input type="email" name="email" placeholder="Enter your email"/>
			<button type="button">Subscribe</button>
		</div>
	</section>
)

export default Subscribe