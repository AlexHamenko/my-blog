import React,{ Component } from 'react'
import PropTypes from 'prop-types'

class TopPostItem extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        text:PropTypes.string.isRequired,
        tag:PropTypes.string,
    }

    render() {
        const {
            id,
            image,
            tag,
            data,
            title,
            text,
        } = this.props

        return (
            <div class="top top1">
                <div class="image-wrapper">
                    <img src={image} alt="top-post-1"/>
                </div>
                <div class="additional-info">
                    <div class="tag">{tag}</div>
                    <div class="data">{data}</div>
                </div>
                <h3>{title}</h3>
          </div>
        )
    }


}

export default TopPostItem