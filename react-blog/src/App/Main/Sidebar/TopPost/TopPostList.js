import React from 'react'
import './TopPost.css';
import TopPostItem from './TopPostItem'

import topPosts from './top-posts';


const TopPostList = () => (
	<section className="top-posts">
		<h2>Top Posts</h2>
		{
			topPosts.map(({
				id,
				image,
				tag,
				data,
				title,
				text,
			}) => (
				<div>
					<TopPostItem 
						id = {id}
						image = {image}
						tag = {tag}
						data = {data}
						title = {title}
						text = {text}
					/>
				</div>
			))
		}
	</section>
)

export default TopPostList