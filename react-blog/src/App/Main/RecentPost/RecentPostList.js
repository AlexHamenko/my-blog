import React from 'react'
import './RecentPost.css';
import RecentPostItem from './RecentPostItem'

import recentPosts from './recent-posts';

const RecentPost = () => (
	<section className="recent-posts">
		{
			recentPosts.map(({
				id,
				image,
				tag,
				data,
				title,
				text,
			}) => (
				<div>
					<RecentPostItem 
						id = {id}
						image = {image}
						tag = {tag}
						data = {data}
						title = {title}
						text = {text}
					/>
				</div>
			))
		}
	</section>
)

export default RecentPost