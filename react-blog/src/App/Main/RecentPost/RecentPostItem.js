import React,{ Component } from 'react'
import PropTypes from 'prop-types'

class RecentPostItem extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        text:PropTypes.string.isRequired,
        tag:PropTypes.string,
    }

    render() {
        const {
            id,
            image,
            tag,
            data,
            title,
            text,
        } = this.props

        return (
            <article class="post post-1">
                <div class="image-wrapper">
                    <img src={image} alt="post-image"/>
                </div>
                <div class="additional-info">
                    <div class="tag">{tag}</div>
                    <div class="data">{data}</div>
                </div>
                <div class="text">
                    <h2>{title}</h2>
                    <p>{text}</p>
                </div>
                <div class="article-footer">
                    <a href="#" class="read-more-button">Read More</a>
                    <div class="share">
                    <p>Share</p>
                    <a href="https://facebook.com"><img src="images/icons/fb-dark.png" alt="facebook"/></a>
                    <a href="https://twitter.com"><img src="images/icons/tw-dark.png" alt="twitter"/></a>
                    <a href="https://www.google.com"><img src="images/icons/g+dark.png" alt="google+"/></a>
                    </div>
                </div>
            </article>
        )
    }


}

export default RecentPostItem